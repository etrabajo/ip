/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ip.vista;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author victor
 */
public class Consulta1 {

    String binario;
    String decimal;
    int notacion;
    int consula;
   
    
    List<Consulta1> tmpConsulta;

    public void consu() {
        tmpConsulta = new ArrayList<Consulta1>();
        tmpConsulta.add(new Consulta1("11111111.00000000.00000000.00000000", "255.0.0.0", 8, 16777216));
        tmpConsulta.add(new Consulta1("11111111.10000000.00000000.00000000", "255.128.0.0", 9, 8388608));
        tmpConsulta.add(new Consulta1("11111111.11000000.00000000.00000000", "255.192.0.0", 10, 4194304));
        tmpConsulta.add(new Consulta1("11111111.11100000.00000000.00000000", "255.224.0.0", 11, 2097152));
        tmpConsulta.add(new Consulta1("11111111.11110000.00000000.00000000", "255.240.0.0", 12, 1048576));
        tmpConsulta.add(new Consulta1("11111111.11111000.00000000.00000000", "255.248.0.0", 13, 524288));
        tmpConsulta.add(new Consulta1("11111111.11111100.00000000.00000000", "255.252.0.0", 14, 262144));
        tmpConsulta.add(new Consulta1("11111111.11111110.00000000.00000000", "255.254.0.0", 15, 131072));
        tmpConsulta.add(new Consulta1("11111111.11111111.00000000.00000000", "255.255.0.0", 16, 65536));
        tmpConsulta.add(new Consulta1("11111111.11111111.10000000.00000000", "255.255.128.0", 17, 32768));
        tmpConsulta.add(new Consulta1("11111111.11111111.11000000.00000000", "255.255.192.0", 18, 16384));
        tmpConsulta.add(new Consulta1("11111111.11111111.11100000.00000000", "255.255.224.0", 19, 8192));
        tmpConsulta.add(new Consulta1("11111111.11111111.11110000.00000000", "255.255.240.0", 20, 4096));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111000.00000000", "255.255.248.0", 21, 2048));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111100.00000000", "255.255.252.0", 22, 1024));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111110.00000000", "255.255.254.0", 23, 512));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111111.00000000", "255.255.255.0", 24, 256));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111111.10000000", "255.255.255.128", 25, 128));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111111.11000000", "255.255.255.192", 26, 64));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111111.11100000", "255.255.255.224", 27, 32));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111111.11110000", "255.255.255.240", 28, 16));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111111.11111000", "255.255.255.248", 29, 8));
        tmpConsulta.add(new Consulta1("11111111.11111111.11111111.11111100", "255.255.255.252", 30, 4));
    }

    public Consulta1() {
        consu();
    }

    public Consulta1(String binario, String decimal, int notacion, int consula) {
        this.binario = binario;
        this.decimal = decimal;
        this.notacion = notacion;
        this.consula = consula;
    }

    public String getBinario() {
        return binario;
    }

    public void setBinario(String binario) {
        this.binario = binario;
    }

    public String getDecimal() {
        return decimal;
    }

    public void setDecimal(String decimal) {
        this.decimal = decimal;
    }

    public int getNotacion() {
        return notacion;
    }

    public void setNotacion(int notacion) {
        this.notacion = notacion;
    }

    public int getConsula() {
        return consula;
    }

    public void setConsula(int consula) {
        this.consula = consula;
    }

    public List<Consulta1> getTmpConsulta() {
        return tmpConsulta;
    }

    public void setTmpConsulta(List<Consulta1> tmpConsulta) {
        this.tmpConsulta = tmpConsulta;
    }

}
